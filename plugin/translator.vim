if exists('g:loaded_translator') && g:loaded_translator
    finish
endif

if !(exists('g:translate_cmd') || executable('trans'))
    echoerr 'You need to specify a command-line translator first.'
    finish
endif
let g:translate_cmd       = get(g:, 'translate_cmd', 'trans -b -t %t -f %f')
let g:translate_player    = get(g:, 'translate_player', 'mplayer')
let g:translate_auto_yank = get(g:, 'translate_auto_yank', 1)

function! s:get_cmd()
  let translate_cmd = get(g:, 'translate_cmd', 'trans -b -f %f -t %t')
  let lang = substitute($LANG . $LC_ALL . $LANGUAGE . 'en', '_.*', '', '')
  let default_from = get(g:, 'translate_default_from', lang)
  let default_to = get(g:, 'translate_default_to', 'en')
  let translate_cmd = substitute(translate_cmd, '%f', default_from, '')
  let translate_cmd = substitute(translate_cmd, '%t', default_to, '')
  echom translate_cmd
  return translate_cmd
endfunction

function! translator#speak(...) range
    let tl = a:0 > 0 ? a:1 : 'en'
    let content = substitute(s:get_selection(), '\s', '%20', 'g')
    let uri = '"http://translate.google.com/translate_tts?ie=UTF-8&tl='.tl.'&q='.content.'"'
    call system(g:translate_player.' '.uri.' &')
endfunction

function! translator#translate(replace) range
    let content = s:get_selection()
    let command = s:get_cmd().' "'.content.'"'
    let result = systemlist(command)
    if g:translate_auto_yank
        let @" = join(result, "\n")
    endif
    if a:replace
        return result[0]
    endif
    echohl String
    for line in result
        echomsg line
    endfor
    echohl None
endfunction

function! translator#translate_replace() range
    let @t = translator#translate(1)
    normal! gv"tp
endfunction

function! s:get_selection()
    let [s:lnum1, s:col1] = getpos("'<")[1:2]
    let [s:lnum2, s:col2] = getpos("'>")[1:2]
    let s:lines = getline(s:lnum1, s:lnum2)
    let s:lines[-1] = s:lines[-1][: s:col2 - (&selection == 'inclusive' ? 1 : 2)]
    let s:lines[0] = s:lines[0][s:col1 - 1:]
    return join(s:lines, ' ')
endfunction

command -range Translate :call translator#translate(0)
command -range TranslateSpeak :call translator#translate_speak()
command -range TranslateReplace :call translator#translate_replace()

let g:loaded_translator = 1
